﻿using UnityEngine;
using System.Collections;

public class Attraction : MonoBehaviour {
    public GameObject pool;

    void Start () {
        pool = GameObject.Find("Pool");
	}

    void Update() {
        foreach (var circle in pool.GetComponent<Circles>().circles) {
            if (circle == null) continue;
            transform.position = Vector2.MoveTowards(transform.position, circle.transform.position,
                circle.GetComponent<Mass>().mass*Time.deltaTime);
        }
    }
}

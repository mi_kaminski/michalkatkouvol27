﻿using UnityEngine;
using System.Collections;

public class Mass : MonoBehaviour {
    public int mass = 1;
    public Circles circlesScript;
    public bool collide = true;

    void Start() {
        transform.localScale = new Vector2(mass * 0.1f, mass * 0.1f);
        Invoke("StartCollide", 0.5f);
    }

    void StartCollide() {
        collide = true;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(!collide) return;
        var otherMass = other.GetComponent<Mass>().mass;
        if (otherMass > mass) return;
        Merge(other.transform, otherMass);
    }

    void Merge(Transform other, int otherMass) {
        if(circlesScript == null) circlesScript = GameObject.Find("Pool").GetComponent<Circles>();
        circlesScript.circles.Remove(other);
        mass += otherMass;
        transform.localScale = new Vector2(mass * 0.06f, mass * 0.06f);
        Destroy(other.gameObject);
        MassCheck();
    }

	void MassCheck () {
	    if (mass < 50) return;
	    circlesScript.Explode(transform.position.x, transform.position.y);
	    circlesScript.circles.Remove(transform);
        Destroy(gameObject);
	}
}

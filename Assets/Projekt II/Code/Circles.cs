﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Circles : MonoBehaviour {
    public List<Transform> circles;
    public int circlesSpawned;
    public Transform circlePrefab;
    public Text circlesCount;

	void Start () {
        InvokeRepeating("SpawnCircle", 0, 0.25f);
	}

    void Update() {
        circlesCount.text = "Circles: " + circlesSpawned.ToString();

        if (circlesSpawned < 250) return;

        CancelInvoke();
        foreach (var circle in circles) {
            circle.GetComponent<CircleCollider2D>().isTrigger = false;
        }
    }

    public void SpawnCircle() {
        circlesSpawned++;
        circles.Add(
            Instantiate(circlePrefab, new Vector2(Random.Range(-80f, 80f), Random.Range(-40f, 40f)),
                Quaternion.identity) as Transform);
    }

    public void Explode(float rangeX, float rangeY) {
        for(int i = 0; i < 50; i++) {
            circles.Add(
                Instantiate(circlePrefab, new Vector2(Random.Range(rangeX - 15f, rangeX + 15f), Random.Range(rangeY - 15f, rangeY + 15f)),
                    Quaternion.identity) as Transform);
            circles[i].GetComponent<Mass>().collide = false;
            circles[i].GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(0, 1), Random.Range(0, 1)) * 100);
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {
    public Turrets turretsScript;
    public Transform parent;
    private bool spawn = true;

    void Start() {
        turretsScript = GameObject.Find("Pool").GetComponent<Turrets>();
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.transform == parent) return;
        spawn = false;
        turretsScript.turrets.Remove(other.gameObject);
        Destroy(other.gameObject);
        Disable();
    }

    void OnEnable() {
        spawn = true;
        Invoke("Disable", Random.Range(0.5f, 2f));
    }

    void Disable() {
        if(spawn) turretsScript.Create(transform);
        gameObject.SetActive(false);
    }

    void OnDisable() {
        CancelInvoke();
    }

    void Update() {
        transform.Translate(Vector3.up * Time.deltaTime * 20);
    }
}

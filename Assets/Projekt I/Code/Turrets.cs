﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Turrets : MonoBehaviour {
    public GameObject turretPrefab;
    public List<GameObject> turrets;
    public Text towersCount;
    private bool max;

    void Awake() {
        turrets = new List<GameObject>();
    }

    void Update() {
        towersCount.text = "Towers: " + turrets.Count.ToString();
    }

    GameObject NewTurret() {
        var turret = Instantiate(turretPrefab) as GameObject;
        turrets.Add(turret);
        return turret;
    }

    void Place(GameObject turret, Transform target) {
        turret.transform.position = target.position;
    }

    public void Create(Transform target) {
        if(max) return;

        if (turrets.Count >= 100) {
            max = true;
            foreach (var t in turrets) {
                var temp = t.GetComponent<Turret>();
                temp.EnableTurret();
            }
            return;
        }
        
        Place(NewTurret(), target);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bullets : MonoBehaviour {
    public GameObject bulletPrefab;
    public List<GameObject> bullets;

    void Awake() {
        bullets = new List<GameObject>();
    }

    GameObject NewBullet() {
        var bullet = Instantiate(bulletPrefab) as GameObject;
        bullet.SetActive(false);
        bullets.Add(bullet);
        return bullet;
    }

    void PrepareBullet(GameObject bullet, Transform target) {
        bullet.GetComponent<Bullet>().parent = target;
        bullet.transform.position = target.position;
        bullet.transform.rotation = target.rotation;
        bullet.SetActive(true);
    }

    public void Fire(Transform target) {
        foreach (var b in bullets) {
            if (b == null) continue;
            if(b.activeInHierarchy) continue;
            PrepareBullet(b, target);
            return;
        }
        PrepareBullet(NewBullet(), target);
    }
}

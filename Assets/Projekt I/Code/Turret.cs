﻿using UnityEngine;
using System.Collections;

public class Turret : MonoBehaviour {
    public bool startImmediately;
    private bool rotating;
    private int shootsCounter;
    private SpriteRenderer spriteRenderer;

	void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
	    if (startImmediately)
	        EnableTurret();
	    else
	        Invoke("EnableTurret", 6f);
	}

    void RotateZ() {
        rotating = true;
        if (shootsCounter >= 12) {
            CancelInvoke();
            DisableTurret();
            return;
        }
        shootsCounter++;
        transform.Rotate(new Vector3(0,0,Random.Range(15,45)));
        GetComponent<Shoot>().ShootNow();
    }

    public void EnableTurret() {
        shootsCounter = 0;
        spriteRenderer.color = Color.red;
        if(!rotating) InvokeRepeating("RotateZ", 0, 0.5f);
    }

    void DisableTurret() {
        rotating = false;
        GetComponent<SpriteRenderer>().color = Color.white;
    }
}

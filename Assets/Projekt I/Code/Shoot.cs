﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {
    public Bullets bulletsScript;

	void Start () {
	    bulletsScript = GameObject.Find("Pool").GetComponent<Bullets>();
	}

    public void ShootNow() {
        bulletsScript.Fire(transform);
    }
}
